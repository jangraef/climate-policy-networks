# Climate Policy Networks



## Next Steps

1. Datensatz überarbeiten 
    * einheitliche Benennung der Organisationen
    * Duplikate identifizieren und kombinieren
    * Organisationen (nodes) in separatem Sheet kodieren
        * Abkürzungen oder numerische IDs vergeben und in Matrizen anpassen
    * Unklare Fälle kodieren
        * Arbeitsgemeinschaften in Einzelaufträge splitten
    Technisch, implement und holistisch ggf re-klassifizieren oder zusammenführen

2. Netzwerkvisualisierung in R verbessern
    * Selektion: ggf. Edges/Nodes vollständig auslassen oder nicht visualisieren?

3. Netzwerkstatistiken berechnen



